-- Leader key set
vim.g.mapleader = " "
-- File Explorer
vim.keymap.set("n", "<leader>fe", vim.cmd.Ex)
-- ctrl-c to go back to normal mode
vim.keymap.set("i", "<C-c>", "<Esc>")
-- ctrl-[hjkl] to change panes
vim.keymap.set("n", "<leader>h", "<cmd>wincmd h<CR>")
vim.keymap.set("n", "<leader>j", "<cmd>wincmd j<CR>")
vim.keymap.set("n", "<leader>k", "<cmd>wincmd k<CR>")
vim.keymap.set("n", "<leader>l", "<cmd>wincmd l<CR>")
