-- Line numbers
vim.opt.nu = true

-- 4 space idents!
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

-- Smart Indent
vim.opt.smartindent = true

-- turning off textwrap
vim.opt.wrap = false

-- Long running undos
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- SearchMods
vim.opt.hlsearch = false
vim.opt.incsearch = true

-- Good Colours
vim.opt.termguicolors = true

-- Leader
vim.g.mapleader = " "
