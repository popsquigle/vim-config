-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use ('wbthomason/packer.nvim')
  
  -- Telescope
  use {
  	'nvim-telescope/telescope.nvim', tag = '0.1.1',
  	requires = { {'nvim-lua/plenary.nvim'} }
  }
  
  -- Colour Theme
  use({
  	'navarasu/onedark.nvim',
	as = 'onedark',
	config = function()
		vim.cmd('colorscheme onedark')
	end
  })
  
  -- Web Devicons
  use ('nvim-tree/nvim-web-devicons')

  -- Finally A statusline!
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'nvim-tree/nvim-web-devicons', opt = true }
  }
  
  -- Treesitter
  use('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})

  -- Harpoon
  use('theprimeagen/harpoon')

  -- UndoTree
  use('mbbill/undotree')

  -- Git
  use('tpope/vim-fugitive')

  -- LSP
  use {
  'VonHeikemen/lsp-zero.nvim',
  branch = 'v2.x',
  requires = {
      -- LSP Support
      {'neovim/nvim-lspconfig'},             -- Required
      {                                      -- Optional
        'williamboman/mason.nvim',
        run = function()
          pcall(vim.cmd, 'MasonUpdate')
        end,
      },
      {'williamboman/mason-lspconfig.nvim'}, -- Optional

      -- Autocompletion
      {'hrsh7th/nvim-cmp'},     -- Required
      {'hrsh7th/cmp-nvim-lsp'}, -- Required
      {'L3MON4D3/LuaSnip'},     -- Required
    }
  }
  
  -- Toggleterm
  use {"akinsho/toggleterm.nvim", tag = '*', config = function()
    require("toggleterm").setup()
  end}
  
  -- Auto-Pairs
  use('jiangmiao/auto-pairs')

  -- Tetris!!!
  use('alec-gibson/nvim-tetris')

  -- Tmux
  use('christoomey/vim-tmux-navigator')

end)
