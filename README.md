# Welcome!
This is my vim (neovim) config! Follow the instructions below and you will be off to the races!

## Notes
For the best experience use atleast nvim 0.8.6 and have ripgrep installed (needed for grep find and telescope)
I reccomend you use nvim 0.9.0 (the most up to date at time of writing) or higher.

## Instructions
1. Install packer (make sure its working)
2. Clone repo
3. Check if I've been stupid and left my packer_compile still in the repo
5. If i left packer_compile in repo, delete it after clone
6. PackerSync
7. Profit?
