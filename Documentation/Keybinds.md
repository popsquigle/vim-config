# Keybinds

## Table of Contents
1. [Navigation](#navigation)
2. [Plugins](#plugins)

## Navigation

### Editor navigation
Editor navigation is just the normal vim keybinds + leader+hjkl for pane navigation, so if you know the stock binds you're golden!

...Except one thing! I binded ctrl-c to go back to normal mode when your in insert mode.

### FileSystem (FS) navigation
This config has three main ways of finding files, you can use netrw (the default vim file explorer) by using:
`<leader> + fe`

To find a specific file you can use telescope! which can be activated by:
`<leader> + ff`

To find a string in a file you can use grep, which is started by using:
`<leader> + gf`

You'll notice that most of these keybinds are mnemonic (I totally didn't have to look that up), that is to say, they make sense. I'll show you!
The netrw keybind is 'fe' for File Explorer,
The telescope keybind is 'ff' for Find File, And finally, the grep keybind is 'gf' for Grep Find.

## Plugins

### Git
To check on your git status, you use the following keybind:
`<leader> + gs`
Though, you have to be in a git repo or it will hurl an error.

### Undo tree
To view the undo tree you simply use the following keybind:
`<leader> + u`
Enjoy!

## End
Thanks for reading this short document on the essential bindings.
Enjoy the config!
